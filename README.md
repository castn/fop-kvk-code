# FOP-KVK-Code

## Sinn und Zweck

In diesem Repository liegt der Code aus den Aufgaben des Klausurvorbereitungskurses für FOP (2022 WiSe).
Es soll dazu dienen die Code-Beispiele oder Fragen aus den Aufgaben zu veranschaulichen und leichter zu verstehen.

Schaut gerne in das Repository, um Dinge zu verstehen, bei denen es bei euch noch Probleme gibt.

## Mitmachen
Das Repository ist public, also bist du herzlich dazu eingeladen mit dran zu arbeiten. Forke einfach das Repository und
öffne einen Pull-Request, wenn du fertig bist, damit auch andere an deinen Lösungen teilhaben können.
Solltest du dir bei deinem Code nicht 100% sicher sein, 
dann erstelle doch ein Issue und man kann sich das gemeinsam anschauen.

Stelle bitte sicher, das du **keinen Code aus den Quiz's oder Klausuren kopierst** und hier einfügst, da ich
die rechtliche Situation nicht genau kenne.