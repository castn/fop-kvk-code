package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe3;

import java.awt.*;

public class X {
    /**
     * static entspricht "KLassenmethode". Sinn und Zweck ist man muss kein Objekt der Klasse erzeugen,
     *     um die Methode aufzurufen.
     *
     *     Es müssen Exception1 und Exception2 zurückgegeben werden, da nur 2 erlaubt sind und Exception verboten ist.
     *     Exception3 ist unnötig, da diese 2 und 3 erbt!
     *
     *     Umsetzung ist wie in Aufgabenstellung beschrieben gemacht, geht aber auch nur mit if statements.
     * @param b
     * @param x
     * @return
     * @throws Exception1
     * @throws Exception2
     */
    public static boolean m(Button[] b, Component x) throws Exception1, Exception2 {
        // Simpler null check, um spätere NullPointerExceptions in der Runtime zu vermeiden!
        if (b == null) {
            throw new Exception1("Error");
        } else if (b.length == 0) {
            throw new Exception2();
        } else {
            if (x == null) {
                throw new Exception3(3.14);
            } else if (x instanceof Button) {
                b[0] = (Button) x;
                return true;
            }
            return false;
        }
    }
}
