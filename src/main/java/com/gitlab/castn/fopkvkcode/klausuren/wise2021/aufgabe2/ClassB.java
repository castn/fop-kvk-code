package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe2;

public class ClassB extends ClassA{
    @Override
    public boolean test(int i) {
        return i % 3 != 0;
    }
}
