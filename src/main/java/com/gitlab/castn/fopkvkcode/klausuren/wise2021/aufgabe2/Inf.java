package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe2;

import java.awt.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

@FunctionalInterface
public interface Inf {
    Window foo(Supplier<Frame> sup, Consumer<Window> con);

    default <T extends Number> void bar(List<? super Number> x, T t) {
        x.add(t);
    }
}
