package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe6;

public class FooClass {

    public static double[] foobar(double[] a) {
        int numberOfValues = 1;

        for (int i = 1; i < a.length; i++) {
            if (a[i-1] > a[i]) {
                numberOfValues++;
            }
        }

        double[] toReturn = new double[numberOfValues];

        // Erste Zahl entspricht immer der ersten aus gegebenen Array.
        toReturn[0] = a[0];

        int next = 1;
        for (int i = 1; i < a.length; i++) {
            if (a[i-1] > a[i]) {
                toReturn[next] = a[i];
                next++;
            }
        }
        return toReturn;
    }
}
