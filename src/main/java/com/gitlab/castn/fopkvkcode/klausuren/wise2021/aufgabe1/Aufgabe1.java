package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe1;

import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe1.umsetzung.A;
import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe1.umsetzung.B;
import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe1.umsetzung.X;
import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe1.verstehen.Tier;
import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe1.verstehen.Vivarium;
import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe1.verstehen.Zoo;

public class Aufgabe1 {
    // Achtung: public Sichtbarkeit in den Methoden der Klassen ist nur notwending, damit es hier im Projekt übersichtlich bleibt!

    //Umsetzung nach Aufgabenstellung
    A beispiel = new B();
    X xBeispiel = beispiel.m();

    // Das Obrige etwas verständlicher und einprägsamer
    Zoo darmstaeterZoo = new Vivarium();
    Tier tierInDarmstadt = darmstaeterZoo.tierInZoo();

}
