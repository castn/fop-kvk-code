package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe4;

// Orginal Code: https://github.com/FOP-2022/FOP-2022-H10-Student/blob/master/src/main/java/h10/ListItem.java
public class ListItem<T> {
    public ListItem<T> next;
    public T key;

    public ListItem() {

    }

    public ListItem(T key) {
        this.key = key;
    }
}
