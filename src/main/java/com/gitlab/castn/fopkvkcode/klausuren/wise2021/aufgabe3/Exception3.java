package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe3;

import java.awt.*;

public class Exception3 extends Exception1 {
    public Exception3(double a) {
        super("Wert: " + a);
    }

    // Wo sich b) befinden soll ist nicht näher spezifiziert, daher hier!

}
