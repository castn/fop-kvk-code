package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe3;

import java.awt.*;

public class Y {

    public boolean m2(Component c) throws Exception {
        Button[] z = new Button[3];
        try {
            return X.m(z, c);
        } catch (Exception3 e) {
            // catch Block von Exception3 muss vor 1 und 2 kommen, da 3 von 1 und 2 erbt un somit in den
            // catch Block von 1 und 2 laufen würde.
            return false;
        } catch (Exception1 | Exception2 e) {
            throw new Exception3(2.71);
        }
    }
}
