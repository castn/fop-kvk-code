package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe4;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class JavaCode {
    /**
     * Code aus Teilaufgabe b)
     *
     * @param lst1
     * @param lst2
     * @param <T>
     * @return
     */
    public <T> List<T> foo(List<T> lst1, List<T> lst2) {
        List<T> returnList = new LinkedList<>();
        Iterator itrLst1 = lst1.iterator();
        Iterator itrLst2 = lst2.iterator();
        int location = 0;

        while (returnList.size() != lst1.size() + lst2.size()) {
            if (itrLst1.hasNext()) {
                returnList.add((T) itrLst1.next());
            }
            if (itrLst2.hasNext()) {
                returnList.add((T) itrLst2.next());
            }
        }

        return returnList;
    }

    public <T> void foo2(ListItem<T> lst1, ListItem<T> lst2, ListItem<T> tail) {

    }
}
