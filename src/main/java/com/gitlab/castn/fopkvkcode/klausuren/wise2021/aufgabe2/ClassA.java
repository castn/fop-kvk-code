package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe2;

import java.awt.*;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import java.util.function.Supplier;




/**
 * Klasse muss abstract sein, sonst müssen alle Methoden von Inf und IntPredicate implementiert werden!
 * Ist nicht gefordert somit muss die Klasse abstract sein. Einfach in IDE ausprobieren... ;)
 */
public abstract class ClassA implements Inf, IntPredicate {

    @Override
    public Window foo(Supplier<Frame> sup, Consumer<Window> con) {
        Frame anyObject = sup.get();
        con.accept(anyObject);

        return anyObject;
    }
}
