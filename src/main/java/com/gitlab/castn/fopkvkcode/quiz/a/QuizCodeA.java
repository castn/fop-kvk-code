package com.gitlab.castn.fopkvkcode.quiz.a;

public class QuizCodeA {
    // Frage 1

    // Frage 2
    public <T> void doNothing (T t) {}

    // Frage 3
    public static int foobar(char[] a) {
        int numberOfChars = 0;

        for(int i = 0; i< a.length; i++) {
            if(Character.isAlphabetic(a[i])){
                numberOfChars++;
            }
        }

        return numberOfChars;
    }

    // Frage 4
    // Kerngedanke des OOP ist: Datenkapselung, Polymorphie, Vererbung, Wiederverwendbarkeit
    // https://openbook.rheinwerk-verlag.de/oop/oop_kapitel_02_001.htm#mj8af1cb6d9a8026216ccb6940e4bfad18
}
