package com.gitlab.castn.fopkvkcode.quiz.w;

import java.util.ArrayList;
import java.util.List;

public class QuizW {


  /**
   * Frage 1
   *
   * Code im Test laufen lassen, um Ergebnisse zu sehen!
   */
  public List<String> checkList(){
    List<String> list = new ArrayList<>();

    list.add("Wurde geadded!");
    list.add("wird überschrieben");

    list.set(1,"Das ist überschrieben worden!");

    return list;
  }

  /**
   * Frage 2
   */

  /**
   * Frage 3
   */

  /**
   * Frage 4 zu Comparable und Comparator
   *
   * Das Interface Comparable gibt der jeweiligen Klasse nur die Möglichkeit verglichen zu werden und
   * zwingt einen compareTo zu implementieren. Diese kann mittels einer Collection sortiert werden.
   * Jedoch kann die Sortierreihenfolge nicht mehr verändert werden (natürliche Ordnung).
   *
   * Das Comparator Interface erlaubt es einem einen eigenen Comparator zu schreiben,
   * in dem ich dann mein zu vergleichendes Objekt nach allen sichtbaren Kriterien vergleichen kann.
   * (name, alter, id). Hier ist es wesentlich variabler. Und ich kann mehrmals die gleiche List mit
   * unterschiedlichen Comparator vergleichen. Ohne das Object zu verändern!
   */

}
