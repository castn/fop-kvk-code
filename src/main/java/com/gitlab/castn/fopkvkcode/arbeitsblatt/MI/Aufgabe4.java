package com.gitlab.castn.fopkvkcode.arbeitsblatt.MI;

public class Aufgabe4 {

  @FunctionalInterface
  public interface A {

    String m(String s);

    default <T extends Number> boolean divisible(T t, int div) {
      return (t instanceof Integer) && (t.intValue() % div == 0);
    }
  }

  public interface B {

    void d(int n);
  }

  // Muss abstract sein, da d aus B nicht implementiert werden soll!
  public abstract class C implements A, B {

    @Override
    public String m(String s) {
      if (s.length() % 2 != 0) {
        return s.toLowerCase();
      } else{
        return s;
      }
    }
  }

  public class D extends C{

    @Override
    public void d(int n){
      n = n*3;
    }
  }


}
