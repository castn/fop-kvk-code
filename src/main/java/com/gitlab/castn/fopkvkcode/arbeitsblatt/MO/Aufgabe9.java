package com.gitlab.castn.fopkvkcode.arbeitsblatt.MO;

import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe4.ListItem;

public class Aufgabe9 {

  public static <T> ListItem<T> foo(ListItem<T> lst1, ListItem<T> lst2) {
    ListItem<T> head = null;
    ListItem<T> tail = null;
    ListItem<T> p1 = lst1;
    ListItem<T> p2 = lst2;

    while (p1 != null && p2 != null) {
      if (p1.key.equals(p2.key)) {
        if (p1.key == p2.key) {
          ListItem<T> tmp = new ListItem<T>();
          tmp.key = p1.key;
          if (head == null) {
            head = tail = tmp;
          } else {
            tail.next = tmp;
            tail = tail.next;
          }
        } else {
          break;
        }
      }

      p1 = p1.next;
      p2 = p2.next;
    }
    return head;
  }


}
