package com.gitlab.castn.fopkvkcode.arbeitsblatt.MO;

public class Aufgabe12 {
  public interface A {
    double m1(int n, char c);
  }

  public interface B extends A {
    String m2();
  }

  public abstract class XY implements A {
    protected long p;

    public XY(long q) {
      p = q;
    }

    abstract public void m3(XY xy);
  }

  public class YZ extends XY implements B {
    public YZ(long r) {
      super(r);
    }

    @Override
    public double m1(int n, char c) {
      return n + (double)c + p;
    }

    @Override
    public String m2() {
      return "Hallo";
    }

    @Override
    public void m3(XY xy) {
      this.p = this.p + xy.p;
    }

  }

}
