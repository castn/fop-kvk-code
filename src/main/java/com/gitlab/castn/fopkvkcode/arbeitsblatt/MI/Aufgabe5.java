package com.gitlab.castn.fopkvkcode.arbeitsblatt.MI;

import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe4.ListItem;
import java.util.List;

public class Aufgabe5 {

  // Nicht die richtige Lösung!
  public ListItem<Integer> foo(List<ListItem<Integer>> lst1, List<ListItem<Integer>> lst2) {
    if (lst1.get(1) == null) {
      return lst1.get(0);
    }
    if (lst2.get(1) == null) {
      return lst2.get(0);
    }
    lst1.remove(0);
    lst2.remove(0);

    return foo(lst1,lst2);
  }


}
