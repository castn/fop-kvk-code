package com.gitlab.castn.fopkvkcode.arbeitsblatt.DI;

import com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe4.ListItem;

public class Aufgabe2 {

  // Kann richtig sein noch nicht getestet...
  public ListItem<Integer> foo(ListItem<Integer> i1, ListItem<Integer> i2) {
    ListItem<Integer> head = new ListItem<Integer>();
    if (i1 == null && i1 == null) {
      return null;
    }

    if (i1.next == null) {
      head.next = i2.next;
    }
    if (i2.next == null) {
      head.next = i1.next;
    } else {
      head.key = i1.key + i2.key;
      head.next = fooHelper(i1.next, i2.next);
    }

    return head;
  }

  private ListItem<Integer> fooHelper(ListItem<Integer> i1, ListItem<Integer> i2) {
    ListItem<Integer> tail = new ListItem<Integer>();

    if (i1.next == null) {
      tail.next = i2.next;
    }
    if (i2.next == null) {
      tail.next = i1.next;
    } else {
      tail.key = i1.key + i2.key;
    }

    return tail;
  }
}
