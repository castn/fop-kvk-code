package com.gitlab.castn.fopkvkcode.arbeitsblatt.MI;

import java.util.Comparator;

public class Aufgabe3 {

  public class Student {

    int enrollmentNumber;
    String firstName;
    String lastName;
  }

  public class StudentComparator implements Comparator<Student> {

    @Override
    public int compare(Student s1, Student s2) {
      int compared = s1.lastName.compareTo(s2.lastName);

      if (compared == 0) {
        // Achte darauf,das compareTo() hier nicht verwendet werden kann, da die enrollmentNumber
        // vom primitiven Datentyp int ist und compareTo() nur bei Integer Typen nutzbar ist.
        if (s1.enrollmentNumber < s2.enrollmentNumber) {
          return -1;
        }
        if (s1.enrollmentNumber == s2.enrollmentNumber) {
          return 0; // Logik? Ist das möglich (außer durch einen Bug)? Eher nicht!
        }
        return 1;
      }
      return compared;
    }
  }
}
