package com.gitlab.castn.fopkvkcode.arbeitsblatt.MO;

@FunctionalInterface
public interface A <X extends Runnable, Y> {
  double value(X x, Y y);

   default boolean yesOrNo(Thread thread){
     return thread instanceof MyThread && ((MyThread) thread).isActive();
  }
}
