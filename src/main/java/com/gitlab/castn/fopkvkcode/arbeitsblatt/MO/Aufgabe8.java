package com.gitlab.castn.fopkvkcode.arbeitsblatt.MO;

public class Aufgabe8 {

  public class X {

  }

  public class Y extends X {

  }

  public class A<T> {

  }

  public class B<T> extends A<T> {

  } // JA Compiler meckert nicht!

  A<X> myVar1 = new A<X>();
  //A<X> myVar2 = new A<Y>(); // Nein geht nicht, da die generischen Typen von dem statischen und
  //                              dynamischen Typen unterschiedlich sind.
  A<X> myVar3 = new B<X>(); // Ja funktioniert, beide generischen Typen sind, gleich siehe Zeile 7!

  public class C<T extends Y> extends A<T> {

  } // Scheinbar ja, aber warum, bzw was passiert da?

  /**
   * Klasse C ist eine generische Klasse, welche die generische Type T hat, dieser ist auf Y und
   * Subtypen beschränkt. Klasse C erweitert die generische Klasse A mit dem generischen Typen T.
   *
   * @param <T>
   */
  public class Calternative<T extends Y> extends A<T> {

  }
  // Klasse C ist eine generische Klasse,
  // welche die generische Type T hat, dieser ist auf Y und Subtypen beschränkt.
  // Klasse C erweitert die generische Klasse A mit dem generischen Typen T.

  A<Y> myVar4 = new C<>();
  A<Y> myVar5 = new Calternative<>();
  //A<X> myVar5 = new C<Y>(); // Ja geht nicht, da die generischen Typen nicht gleich sind!


}
