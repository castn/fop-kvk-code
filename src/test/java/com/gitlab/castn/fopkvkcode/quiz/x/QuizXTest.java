package com.gitlab.castn.fopkvkcode.quiz.x;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import org.junit.jupiter.api.Test;

class QuizXTest {

  @Test
  void testAufgabe1(){
    QuizX testee = new QuizX();
    int[] array = {1,2,3,4};

    int[] output = QuizX.Aufgabe1.foobar(array);
    System.out.println(Arrays.toString(output));
  }

}