package com.gitlab.castn.fopkvkcode.quiz.a;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class QuizCodeATest {

  @Test
  void foobar() {
    char[] randomChars = {'a', 'A', '!', '1', 's'};

    int actual = QuizCodeA.foobar(randomChars);

    assertEquals(3, actual);
  }

  @Test
  void test() {
    String p1 = "Hallo";
    String p2 = "Hallo";
    String p3 = "hallo";
    Integer i1 = 100;
    Integer i2 = 50 + 50;

    i1.compareTo(i2);

    double d1 = 2.34568;
    char d2 = '3';

    int rd = (int) (d1 + d2);

    var r1 = p1.equals(p2);
    var r2 = p1 == p2;
    var r3 = p1.equals(p3);
    var r4 = p1 == p3;

    var r5 = i1.equals(i2);
    var r6 = i1 == i2;

  }
}