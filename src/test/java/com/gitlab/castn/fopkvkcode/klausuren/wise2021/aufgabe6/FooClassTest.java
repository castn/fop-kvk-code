package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FooClassTest {

    @Test
    void foobar() {
        double[] given = {3, 4, 2, 5, 6, 8, 9, 8, 9};
        double[] expected = {3,2,5,8};

        double[] actual = FooClass.foobar(given);

        assertEquals(expected, actual);
    }
}