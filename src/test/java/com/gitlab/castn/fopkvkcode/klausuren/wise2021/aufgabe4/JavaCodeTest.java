package com.gitlab.castn.fopkvkcode.klausuren.wise2021.aufgabe4;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JavaCodeTest {
    JavaCode testee = new JavaCode();

    @Test
    void fooTest() {
        // Test setup
        String[] intAsString = {"1", "2", "3", "4", "5", "6", "7", "8"};
        String[] strings = {"a", "b", "c", "d", "e"};
        String[] expectedOutput = {"1", "a", "2", "b", "3", "c", "4", "d", "5", "e", "6", "7", "8"};
        List<String> expected = Arrays.asList(expectedOutput);
        // Test setup Ende

        List<String> actual = testee.foo(Arrays.asList(intAsString), Arrays.asList(strings));

        // Schaut euch expectedOutput an, muss gleich actual sein, da sonst der Test fehlschlägt!
        assertEquals(expected, actual);
    }

}